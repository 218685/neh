#include <iostream>
#include <fstream>
#include <algorithm>
#include <Windows.h>

using namespace std;

int Cmax(int howManyTasks, int howManyMachines, int *taskData, int *order);

int Cmax(int howManyTasks, int howManyMachines, int *taskData, vector<int> &order);

int NEHTest(int howManyTasks, int howManyMachines, int *taskData);

int NEH(int howManyTasks, int howManyMachines, int *taskData);

struct task {
	int WGHT;
	int ID;

	bool operator<(const task &druga) const {
		return (WGHT < druga.WGHT);
	}

	bool operator>(const task &druga) const {
		if (WGHT == druga.WGHT)
			return (ID < druga.ID);
		else
			return (WGHT > druga.WGHT);
	}
};

int main(int argc, char **argv) {

	__int64 start, stop, freq;
	fstream dataFile;
	string nehDataName = "neh.data.txt", nehDataShort = "neh.data.short.txt";

	dataFile.open(nehDataName);

	if (!dataFile.good()) {
		cout << "Nie można otworzyć pliku!\nNaciśnij dowolny klawisz, aby zakończyć..." << endl;
		cin.get();
		return -1;
	}

	string dataSetName, tempString;
	int howManyTasks, howManyMachines, tempInt;
	double calkowityCzas=0, pojedynczyCzas;

	while (!dataFile.eof()) {
		dataFile >> dataSetName;

		cout << endl << dataSetName << " ";
		dataFile >> howManyTasks >> howManyMachines;

		cout << howManyTasks << " " << howManyMachines << endl;
		QueryPerformanceCounter((LARGE_INTEGER*)&start);
        int *taskData = new int[howManyTasks*howManyMachines];

        for(int i=0; i< howManyTasks*howManyMachines; ++i)
            dataFile >> taskData[i];
		//NEHTest(howManyTasks,howManyMachines,taskData);
		NEH(howManyTasks, howManyMachines, taskData);

		QueryPerformanceCounter((LARGE_INTEGER*)&stop);
		QueryPerformanceFrequency((LARGE_INTEGER*)&freq);
		pojedynczyCzas = ( (double) (stop-start) )/freq;
		calkowityCzas += pojedynczyCzas;
		cout << "Czas: " << pojedynczyCzas << " s" << endl;

		delete[] taskData;

		dataFile >> tempString >> tempInt;
		for (int i = 0; i < howManyTasks; i++) {
			dataFile >> tempInt;
		}
	}

	dataFile.close();

	cout << "Całkowity czas: " << calkowityCzas << " s" << endl;
	cin.get();

	return 0;
}

int Cmax(int howManyTasks, int howManyMachines, int *taskData, int *order) {

	int *m = new int[howManyMachines + 1];

	for (int j = 0; j < howManyMachines + 1; j++)
		m[j] = 0;

	for (int i = 0; i < howManyTasks; i++) {
		for (int j = 1; j < howManyMachines + 1; j++) {
			m[j] += taskData[(order[i] - 1) * howManyMachines + j - 1];
			m[j] = max(m[j], m[j - 1] + taskData[(order[i] - 1) * howManyMachines + j - 1]);
			//cout << "m["<<j<<"]=" << m[j] << " ";
		}
	}

	int cmax = m[howManyMachines];
	delete[] m;

	return cmax;
}

int Cmax(int howManyTasks, int howManyMachines, int *taskData, vector<int> &order) {

	int *maszyna = new int[howManyMachines + 1];

	for (int j = 0; j < howManyMachines + 1; j++)
		maszyna[j] = 0;

	for (int i = 0; i < howManyTasks; i++) {
		for (int j = 1; j < howManyMachines + 1; j++) {
			maszyna[j] += taskData[(order[i] - 1) * howManyMachines + j - 1];
			maszyna[j] = max(maszyna[j], maszyna[j - 1] + taskData[(order[i] - 1) * howManyMachines + j - 1]);
			//cout << "maszyna["<<j<<"]=" << maszyna[j] << " ";
		}
	}

	int cmax = maszyna[howManyMachines];
	delete[] maszyna;

	return cmax;
}

int NEHTest(int howManyTasks, int howManyMachines, int *taskData) {

	//wczytanie wag i numerów poszczegolnych zadan
	task *zadania = new task[howManyTasks];

	int k = 0, sumaTmp;
	for (int i = 0; i < howManyTasks * howManyMachines; i += howManyMachines) {
		sumaTmp = 0;
		for (int j = 0; j < howManyMachines; j++)
			sumaTmp += taskData[i + j];
		//cout << "suma: " <<sumaTmp << endl;
		zadania[k].WGHT = sumaTmp;
		zadania[k].ID = ++k;
	}
	//sortowanie wg najdłuższych zadania
	sort(zadania, zadania + howManyTasks, greater<task>());

	vector<int> pi((unsigned long) howManyTasks);
	cout << "Permutacja pierwsza: ";
	for (int j = 0; j < howManyTasks; j++) {

		pi[j] = zadania[j].ID;
		cout << pi[j] << " ";
	}
	cout << endl;
	delete[] zadania;

	int best, cmax, bestCmax = 0;

	for (int i = 2; i <= howManyTasks; i++) {

		bestCmax = 99999999, best = 0;
		cout << "i=" << i << " pi[i]=" << pi[i - 1] << endl;

		cout << " i=" << i << "	Kolejnosc: ";
		for (int k = 0; k < i; k++)
			cout << pi[k] << " ";
		cmax = Cmax(i, howManyMachines, taskData, pi);
		cout << endl << " 	cmax =" << cmax << endl;
		if (cmax < bestCmax) {
			bestCmax = cmax;
			cout << "	bestCmax=" << bestCmax << " ";
			best = i - 1;
			cout << "best= " << best << endl;
		}

		for (int p = i - 1; p > 0; p--) {

			swap(pi[p], pi[p - 1]);
			//wyswietl order
			cout << " p=" << p << "	Kolejnosc: ";
			for (int k = 0; k < i; k++)
				cout << pi[k] << " ";

			cmax = Cmax(i, howManyMachines, taskData, pi);
			cout << endl << " 	cmax =" << cmax << endl;
			if (cmax < bestCmax) {
				bestCmax = cmax;
				cout << "	bestCmax=" << bestCmax << " ";
				best = p - 1;
				cout << "best= " << best << endl;
			}
		}
		if (best != 0) {

			rotate(pi.begin(), pi.begin() + 1, pi.begin() + best + 1);  // O(howManyTasks)

			cout << "swap " << pi[best] << " z " << pi[0] << endl;
		}
		cout << " Kolejnosc najlepsza: ";
		for (int k = 0; k < i; k++)
			cout << pi[k] << " ";
		cout << endl;
	}
	return bestCmax;
}

int NEH(int howManyTasks, int howManyMachines, int *taskData) {

	//wczytanie wag i numerów poszczegolnych zadan
	task *zadania = new task[howManyTasks];

	int k = 0, sumaTmp;
	for (int i = 0; i < howManyTasks * howManyMachines; i += howManyMachines) {
		sumaTmp = 0;
		for (int j = 0; j < howManyMachines; j++)
			sumaTmp += taskData[i + j];
		zadania[k].WGHT = sumaTmp;
		zadania[k].ID = ++k;
	}
	//sortowanie wg najdłuższego zadania
	sort(zadania, zadania + howManyTasks, greater<task>());

	vector<int> pi((unsigned long) howManyTasks);
	for (int j = 0; j < howManyTasks; j++)
		pi[j] = zadania[j].ID;

	delete[] zadania;

	int best, cmax, bestCmax = 0;

	for (int i = 2; i <= howManyTasks; i++) {

		bestCmax = 9999999, best = 0;
		cmax = Cmax(i, howManyMachines, taskData, pi);

		if (cmax < bestCmax) {
			bestCmax = cmax;
			best = i - 1;
		}
		for (int p = i - 1; p > 0; p--) {
			swap(pi[p], pi[p - 1]);
			cmax = Cmax(i, howManyMachines, taskData, pi);
			if (cmax <= bestCmax) {
				bestCmax = cmax;
				best = p - 1;
			}
		}
		//zamiana badanej komorki na najlepsze dla niej miejsce
		if (best != 0) {
			rotate(pi.begin(), pi.begin() + 1, pi.begin() + best + 1); // O(howManyTasks)
			//cout << "swap " << pi[best] << " z " << pi[0] << endl;
		}

	}
	cout << "NEH: " << bestCmax << endl;
	//wypisanie kolejnosci
	for (int k = 0; k < howManyTasks; k++)
		cout << pi[k] << " ";
	cout << endl;
	return bestCmax;
}